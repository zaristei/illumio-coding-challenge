# Illumio Coding Challenge

I have included my solution Firewall.py, along with my test file test1.csv, which includes all the example rules from the prompt document.
This algorithm has a runtime of O(N) for N rules for both firewall initialization and for accept_packet. The main idea benefit of this approach is it avoids having to iterate through the 256^4 possible IP values by storing rule indexes instead, and iterating through the indices that fulfill the other firewall columns to see if a matching IP rule exists. The worst case occurs when presented with an asymmetricly distributed ruleset across the range of possible rules.
If I had more time, I would've generated a csv of 500,000 randomly selected rules and implemented a randomized tests based on that ruleset. I also would have tried to reduce the amount of iteration over port number by checking for asymmetric port distribution of the rules.
I'd be happy to answer any questions you may have about my code. I hope to get the chance to meet in person! Thanks for the Oportunity!

I rank my interest in the different teams as follows (most favored to least):
1) Data
2) platform
3) policy