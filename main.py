import pandas as pd

class Firewall:
  def __init__(self, path):
    
    # Extract csv to np array
    data = pd.read_csv(path, header=None).values

    # IP addresses
    self.ips = data[:,3]

    # direction and protocol combined into 1 category
    # Row indexes of rules in CSV file are stored in their corresponding directionprotocol, port category
    self.rules = {'inboundtcp': [set() for i in range(65535)], 'inboundudp': [set() for i in range(65535)], 'outboundtcp': [set() for i in range(65535)], 'outboundudp': [set() for i in range(65535)]}
    for i in range(len(data)):
      if data[i][2].isdigit():
        port = int(data[i][2])
        port = [int(port), int(port)]
      else: 
        port = data[i][2].split('-')
        port = [int(port[0]), int(port[1])]
      # Rules with port ranges get their index inserted across the whole range
      for p in range(port[0]-1,port[1]):
        self.rules[data[i][0] + data[i][1]][p].add(i)



  def accept_packet(self, direction, protocol, port, ip_address):
    for i in self.rules[direction + protocol][port-1]: # Searches the bin defined by the first 3 categories to look for matching IP or IP range
      if ip_address == self.ips[i]:
        return True
      elif '-' not in self.ips[i]:
        continue
      else:
        iprange = self.ips[i].split('-')
        ip_address = self._ip_parse(ip_address)
        if self._ip_parse(iprange[0]) <= ip_address and self._ip_parse(iprange[1]) >= ip_address:
          return True
    return False
      
  def _ip_parse(self, ip_addr):
    # Parses IP from string to int for comparing
    ip_parts = ip_addr.split('.')
    return int(ip_parts[0])*10**9 + int(ip_parts[1])*10**6 + int(ip_parts[2])*10**3 + int(ip_parts[3])